"use strict";
exports.__esModule = true;
var estudiante_model_1 = require("./model/estudiante.model");
var estudiante_controller_1 = require("./controller/estudiante.controller");
var estudiante1 = new estudiante_model_1.Estudiante(1, 'Javier', 'Huertas', '2015001', 'Kinal es la mera');
var estudiante2 = new estudiante_model_1.Estudiante(2, 'Hugo', 'Palma', '2015002', 'Kinal es la tos');
var estudiante3 = new estudiante_model_1.Estudiante(3, 'Josue', 'Hernandez', '2015003', 'Kinal es la mera');
var estudiante4 = new estudiante_model_1.Estudiante(4, 'Angel', 'Lopez', '2015004', 'Cuando me va a enviar mi correo profe');
var controlador = new estudiante_controller_1.EstudianteController();
//Agregar
controlador.addEstudiantes(estudiante1);
controlador.addEstudiantes(estudiante2);
controlador.addEstudiantes(estudiante3);
controlador.addEstudiantes(estudiante4);

// Mostrando todos
console.log(JSON.stringify(controlador.getAll()));


