import { Estudiante } from './model/estudiante.model';
import { EstudianteController } from './controller/estudiante.controller';

let estudiante1 = new Estudiante(1, 'Javier', 'Huertas', '2015001', 'Kinal es la mera');
let estudiante2 = new Estudiante(2, 'Hugo', 'Palma', '2015002', 'Kinal es la tos');
let estudiante3 = new Estudiante(3, 'Josue', 'Hernandez', '2015003', 'Kinal es la mera');
let estudiante4 = new Estudiante(4, 'Angel', 'Lopez', '2015004', 'Cuando me va a enviar mi correo profe');

let controlador = new EstudianteController();

//Agregar
controlador.addEstudiantes(estudiante1);
controlador.addEstudiantes(estudiante2);
controlador.addEstudiantes(estudiante3);
controlador.addEstudiantes(estudiante4);

// Mostrando todos
console.log(JSON.stringify(controlador.getAll()));

//Editando 1

let estudianteEditado = controlador.updateEstudiante(new Estudiante(2, 'Otto', 'Chamo',
     '2015373','Kinal es la tox'));

console.log('Estudiante editado: '+ JSON.stringify(estudianteEditado));

// eliminando
let mensaje = controlador.eliminarEstudiante(1);

console.log(mensaje);

//Buscando uno

let estudianteBuscar = controlador.buscarEstudiante(2);

console.log('estudiante que se busco: ' + JSON.stringify(estudianteBuscar));