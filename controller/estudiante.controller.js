"use strict";
exports.__esModule = true;
var EstudianteController = (function () {
    function EstudianteController() {
        this.estudiantes = new Array();
    }
    EstudianteController.prototype.addEstudiantes = function (estudiante) {
        this.estudiantes.push(estudiante);
    };
    EstudianteController.prototype.updateEstudiante = function (estudiante) {
        var modificar = this.estudiantes.filter(function (e) { return e.getId() == estudiante.getId(); });
        modificar[0].setNombre(estudiante.getNombre());
        modificar[0].setApellido(estudiante.getApellido());
        modificar[0].setCarne(estudiante.getCarne());
        modificar[0].setComentario(estudiante.getComentario());
    };
    EstudianteController.prototype.eliminarEstudiante = function (id) {
        var x = this.estudiantes.filter(function (c) { return c.getId() == id; })[0];
        var index = this.estudiantes.indexOf(x);
        this.estudiantes.splice(index, 1);
    };
    EstudianteController.prototype.buscarEstudiante = function (id) {
        return this.estudiantes.filter(function (s) { return s.getId() == id; })[0];
    };
    EstudianteController.prototype.getAll = function () {
        return this.estudiantes;
    };
    return EstudianteController;
}());
exports.EstudianteController = EstudianteController;
