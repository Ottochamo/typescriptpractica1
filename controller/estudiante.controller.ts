import { Estudiante } from '../model/estudiante.model';

class EstudianteController {
    private estudiantes: Array<Estudiante>;

    constructor() {
        this.estudiantes = new Array<Estudiante>();
    }

    public addEstudiantes(estudiante: Estudiante) {
        this.estudiantes.push(estudiante);
    }

    public updateEstudiante(estudiante: Estudiante): Estudiante {
        let modificar: Estudiante = this.estudiantes
            .filter(e => e.getId() == estudiante.getId())[0];
        modificar.setNombre(estudiante.getNombre());
        modificar.setApellido(estudiante.getApellido());
        modificar.setCarne(estudiante.getCarne());
        modificar.setComentario(estudiante.getComentario());

        return modificar;

    }

    public eliminarEstudiante(id: number): string {
        let x = this.estudiantes.filter(c => c.getId() == id)[0];
        if (x !== undefined) {
            let index = this.estudiantes.indexOf(x);
            this.estudiantes.splice(index, 1);
            return 'Estudiante eliminado con exito';
        } 
        return 'No se pudo eliminar';
    }

    public buscarEstudiante(id: number): Estudiante {
        return this.estudiantes.filter(s => s.getId() == id)[0];
    }

    public getAll(): Array<Estudiante> {
        return this.estudiantes;
    }

}

export { EstudianteController };