interface Persona {
    idEstudiante: number,
    nombre: string,
    apellido: string,
    carnet: string,
    comentario: string

}

export { Persona }; 
