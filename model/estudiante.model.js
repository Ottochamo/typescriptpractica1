"use strict";
exports.__esModule = true;
var Estudiante = (function () {
    function Estudiante(id, nombre, apellido, carnet, comentario) {
        this.idEstudiante = id;
        this.nombre = nombre;
        this.apellido = apellido;
        this.carnet = carnet;
        this.comentario = comentario;
    }
    Estudiante.prototype.setId = function (id) {
        this.idEstudiante = id;
    };
    Estudiante.prototype.getId = function () {
        return this.idEstudiante;
    };
    Estudiante.prototype.setNombre = function (nombre) {
        this.nombre = nombre;
    };
    Estudiante.prototype.getNombre = function () {
        return this.nombre;
    };
    Estudiante.prototype.setApellido = function (apellido) {
        this.apellido = apellido;
    };
    Estudiante.prototype.getApellido = function () {
        return this.apellido;
    };
    Estudiante.prototype.setCarne = function (carnet) {
        this.carnet = carnet;
    };
    Estudiante.prototype.getCarne = function () {
        return this.carnet;
    };
    Estudiante.prototype.setComentario = function (comentario) {
        this.comentario = comentario;
    };
    Estudiante.prototype.getComentario = function () {
        return this.comentario;
    };
    return Estudiante;
}());
exports.Estudiante = Estudiante;
