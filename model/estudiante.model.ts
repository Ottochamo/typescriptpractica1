import { Persona } from '../interface/persona.interface';

class Estudiante implements Persona {
    idEstudiante: number;
    nombre: string;
    apellido: string;
    carnet: string;
    comentario: string;

    constructor(id:number, nombre: string, apellido: 
        string, carnet: string, comentario: string) {
            this.idEstudiante = id;
            this.nombre = nombre;
            this.apellido = apellido;
            this.carnet = carnet;
            this.comentario = comentario;
            
    }

    public setId(id: number) {
        this.idEstudiante = id;

    }

    public getId(): number {
        return this.idEstudiante;

    }

    public setNombre(nombre: string) {
        this.nombre = nombre;

    }

    public getNombre(): string {
        return this.nombre;
    }

    public setApellido(apellido: string) {
        this.apellido = apellido;
    }

    public getApellido(): string {
        return this.apellido;
    }

    public setCarne(carnet: string) {
        this.carnet = carnet;
    }

    public getCarne(): string {
        return this.carnet;
    }

    public setComentario(comentario: string) {
        this.comentario = comentario;
    }
 
    public getComentario(): string {
        return this.comentario;
    }

}

export { Estudiante };